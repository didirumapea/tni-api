const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('disaster_files').del()
        .then(function () {
            // Inserts seed entries
            return knex('disaster_files').insert([
                {
                    id: 1,
                    report_disaster_org_id: 1,
                    type_files: 'video',
                    path_url: 'organisasi-2.png',
                },
                {
                    id: 2,
                    report_disaster_org_id: 2,
                    type_files: 'image',
                    path_url: 'organisasi-2.png',
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
