const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('master_content_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('master_content_type').insert([
                {
                    id: 1,
                    name: 'Bagaimana Jika',
                    slug: 'bagaimana-jika',
                },
                {
                    id: 2,
                    name: 'Quiz',
                    slug: 'quiz',
                },
                {
                    id: 3,
                    name: 'Informasi',
                    slug: 'informasi',
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
