const PDFDocument = require('pdfkit');
const fs = require('fs');
const readline = require('readline');
const mkdirp = require('mkdirp');
const basepath =  __dirname.replace('plugins', '');
const cfg = require('../config');
const moment = require('moment/moment');
const ExcelJS = require('exceljs'); // problem server shuould kill with `killall node`
const excelToJson = require('convert-excel-to-json');
// const changeCase = require('change-case')

const outputPath = cfg.assetPath + 'billnet-files/assets/files/output/';

function LAYOUT() {
    this.indexAllKurir = {}; // index all kurir by code
    this.indexAllKurir2 = {}; // index all kurir by name
    this.gX = 0;
    this.gY = 5;

    this.coverSample = (doc, data) => {
        // console.log(moment(data.fh.periode).format('DD-MM-YYYY'))
        // const kurir = data.fh.listKurir.filter(x => x.aliases === kurir_name)[0]
        // console.log(kurir)
        doc
            .moveTo(0, 75)                               // set the current point
            .lineTo(380, 75)
            .moveTo(0, 125)                               // set the current point
            .lineTo(380, 125)
            // .moveTo(0, 265)                               // set the current point
            // .lineTo(380, 265)
            .stroke()
            .font('arial_bold')
            .fontSize(13)
            .text(`SAMPLE LABEL MEGA Card Cyc ${moment(data.fh.periode).format('DD-MM-YYYY')}`, 50, 55)
            .fontSize(32)
            // .text(kurir.filename, 50, 80)
            // .text(data.am.filenamePDF, 50, 80)
            .fontSize(25)
            .text(`SAMPLE PRODUK = ${data.fh.detail.name.toUpperCase()}`, 50, 85)
            // .text(`MATERIAL = ${data.fh.detail.material}`, 50, 155)
            // .text(`KURIR = ${kurir.code}-${kurir.fullname}`, 50, 170)
            // .text(`Env = (${data.fh.seq_user_per_pdf[kurir.aliases]}) | Pages = (${data.fh.rangePDF[kurir_name].count-1})`, 50, 185)
    }
    //
    this.cover = (doc, data, kurir_name) => {
        // console.log(moment(data.fh.periode).format('DD-MM-YYYY'))
        const kurir = data.fh.listKurir.filter(x => x.aliases === kurir_name)[0]
        // console.log(kurir)
        doc
            .moveTo(0, 75)                               // set the current point
            .lineTo(380, 75)
            .moveTo(0, 125)                               // set the current point
            .lineTo(380, 125)
            .moveTo(0, 265)                               // set the current point
            .lineTo(380, 265)
            .stroke()
            .font('arial_bold')
            .fontSize(13)
            .text(`LABEL MEGA Card Cyc ${moment(data.fh.periode).format('DD-MM-YYYY')}`, 50, 55)
            .fontSize(32)
            .text(kurir.filename, 50, 80)
            // .text(data.am.filenamePDF, 50, 80)
            .fontSize(13)
            .text(`PRODUK = ${data.fh.detail.name.toUpperCase()}`, 50, 140)
            .text(`MATERIAL = ${data.fh.detail.material}`, 50, 155)
            .text(`KURIR = ${kurir.code}-${kurir.fullname}`, 50, 170)
            .text(`Env = (${data.fh.seq_user_per_pdf[kurir.aliases]}) | Pages = (${data.fh.rangePDF[kurir_name].count-1})`, 50, 185)
    }
    // e.g DASHLINE, TOP BLOCK
    this.header = (doc, data) => {
        const headY = this.gY;
        // console.log(moment(data.tgl_tagihan).format("DD MMM YYYY").toUpperCase())
        //region DESIGN
        doc.roundedRect(35, 55 + headY, 105, 30, 5)
            .stroke();
        doc.roundedRect(145, 55 + headY, 70, 30, 5)
            .stroke();
        doc.roundedRect(220, 55 + headY, 70, 30, 5)
            .stroke();
        doc.roundedRect(295, 55 + headY, 90, 30, 5)
            .stroke();
        doc.roundedRect(390, 55 + headY, 90, 30, 5)
            .stroke();


// 1
        const fill = '#d2d3d5'
        doc.roundedRect(35.5, 55.5 + headY, 104, 8, 4.5)
        // .fill(fill);
        doc.rect(35.5, 60 + headY, 104, 8)
        // .fill(fill);
// 2
        doc.roundedRect(145.5, 55.5 + headY, 69, 8, 4.5)
        // .fill(fill);
        doc.rect(145.5, 60 + headY, 69, 8)
        // .fill(fill);
// 3
        doc.roundedRect(220.5, 55.5 + headY, 69, 8, 4.5)
        // .fill(fill);
        doc.rect(220.5, 60 + headY, 69, 8)
        // .fill(fill);
// 4
        doc.roundedRect(295.5, 55.5 + headY, 89, 8, 4.5)
        // .fill(fill);
        doc.rect(295.5, 60 + headY, 89, 8)
        // .fill(fill);
// 5
        doc.roundedRect(390.5, 55.5 + headY, 89, 8, 4.5)
        // .fill(fill);
        doc.rect(390.5, 60 + headY, 89, 8)
            .fill(fill)
            .stroke();

        // DASHED LINE WITH CROPS ICON
        const y1 = 72.5 + headY;
        const x1 = 30;
        doc
            .moveTo(x1, 20 + y1)                               // set the current point
            .lineTo(480, 20 + y1)                            // draw a line
            .dash(7, {space: 5})
            .stroke()
            .undash();

        // Add an image, constrain it to a given size, and center it vertically and horizontally
        doc.image('public/images/bank-mega/cuts.png', 17, 82 + headY, {
            fit: [20, 20],
            align: 'center',
            valign: 'center'
        });
        //endregion
        //region TEXT
        doc
            //
            .fillColor('black')
            .font('Times-Roman')
            .fontSize(7)
            // TITLE
            .text('Nomor Kartu Anda', 35, 60 + headY, {
                width: 105,
                align: 'center'
            })
            .text('Tanggal Tagihan', 146, 60 + headY, {
                width: 70,
                align: 'center'
            })
            .text('Tanggal Jatuh Tempo', 221, 60 + headY, {
                width: 70,
                align: 'center'
            })
            .text('Total Tagihan (Rp)', 296, 60 + headY, {
                width: 90,
                align: 'center'
            })
            .text('Pembayaran Minimal (Rp)', 391, 60 + headY, {
                width: 90,
                align: 'center'
            })
            // VALUE
            .font('letter_gothic')
            .fontSize(8)
            // NOMER KARTU ANDA
            .text(card_strip_format(data.ah.card_no), 35, 72.5 + headY, {
                width: 105,
                align: 'center'
            })
            // TANGGAL TAGIHAN
            .text(moment(data.ah.tgl_tagihan).format("DD MMM YYYY").toUpperCase(), 146, 72.5 + headY, {
                width: 70,
                align: 'center'
            })
            // TANGGAL JATUH TEMPO
            .text(moment(data.ah.tgl_jatuh_tempo).format("DD MMM YYYY").toUpperCase(), 221, 72.5 + headY, {
                width: 70,
                align: 'center'
            })
            // TOTAL TAGIHAN
            .text(remove_plus_minus(data.ah.total_tagihan), 296, 72.5 + headY, {
                width: 90,
                align: 'center'
            })
            // PEMBAYARAN MINIMAL
            .text(remove_plus_minus(data.ah.pembayaran_minimal), 391, 72.5 + headY, {
                width: 90,
                align: 'center'
            })
        //endregion
    }
    // e.g ADDRESS, NOTIFICATION
    this.subHeader = (doc, data) => {
        // console.log(data.fh.seq_user_per_pdf[data.fh.kurir.name])
        const x = 0; // width
        const subY = 40 + this.gY; // height
        // const barcodeDate = moment(data.fh.periode).format('DDMMYY')
        // const printDate = moment().format('DD')
        const barcode = moment(data.fh.periode).format('DDMMYY') + moment().format('DD') + data.fh.detail.jenis + '0' + data.ah.user_barcode_number;
        // NAME + ADDRESS HEADER
        doc
            .fillColor('black')
            .fontSize(8)
            .font('letter_gothic_bold')
            // name
            .text(data.ah.name, x + 35, subY + 66)
            .font('letter_gothic')
            // COMPANY NAME
            .text(data.ah.company_name, x + 35, subY + 73)
            // ADDR 1
            .text(data.ah.addr1, x + 35, subY + 80)
            // ADDR 2
            .text(data.ah.addr2, x + 35, subY + 87)
            // ADDR 3
            .text(data.ah.addr4, x + 35, subY + 94)
            // ADDR 4
            .text(data.ah.addr5, x + 35, subY + 101)
            // POSCODE
            .text(data.ah.pos_code, x + 140, subY + 101)
            // BARCODE
            .font('code_3_of_9')
            .fontSize(15)
            .text(`*${barcode}*`, x + 35, subY + 109)
            // SEQ NUMBER
            .font('arial_bold')
            .fontSize(6)
            .text(`${data.fh.seq_number_cetak.toString().padStart(6, '0')} - ${barcode} / ${data.am.filenamePDF} (${this.indexAllKurir[data.fh.kurir.name]} / ${data.fh.seq_user_per_pdf[data.fh.kurir.aliases]})`, x + 35, subY + 124)
            // MESSAGE TEXT
            .fontSize(7)
            .text(data.sh.message.replace(/\s+/g, ' '), 300, 70 + subY, {
                width: 175,
                align: 'center'
            })
            // KUALITAS KREDIT
            .fontSize(7)
            .text(`KUALITAS KREDIT = ${data.ah.kualitas_kredit.toUpperCase()}`, 265, 115 + subY, {
                width: 250,
                align: 'center'
            })
            // HALAMAN
            .font('letter_gothic')
            .fontSize(6)
            .text(`Halaman : ${data.ah.halaman < 9 ? '0'+data.ah.halaman : data.ah.halaman}`, x + 440, 140 + subY)
    }
    // e.g TRANSACTION
    this.body = (doc, data) => {
        // INIT BODY DESIGN
        const temptRow = data.fh.detail.name === 'cashline' ? -20 : 0;
        const x = 4;
        const bodY = this.gY;
        const fill = '#d2d3d5'
        // ('x', 'x_to', 'y', 'y_to')
        // BIG RECTANGLE & SUB HEADER
        doc
            // HEADER
            .rect(20, 188 + bodY, 465, 445)
            .lineWidth(1.5)
            .stroke()
            // SUB HEADER
            .roundedRect(25 + x, 192.5 + bodY, 60, 15, 5)
            .roundedRect(90 + x, 192.5 + bodY, 60, 15, 5)
            .roundedRect(155 + x, 192.5 + bodY, 233, 15, 5)
            .roundedRect(393 + x, 192.5 + bodY, 80, 15, 5)
            .fillAndStroke(fill);
        doc
            .font('arial')
            .fontSize(10)
            .fillColor('black')
            .text('Lembar Penagihan', 205, 175 + bodY)
            .font('Times-Roman')
            .fontSize(8)
            .text('Tgl. Transaksi', 25 + x, 196.5 + bodY, {
                width: 60,
                align: 'center'
            })
            .text('Tgl. Pembukuan', 90 + x, 196.5 + bodY, {
                width: 60,
                align: 'center'
            })
            .text('Keterangan', 155 + x, 196.5 + bodY, {
                width: 233,
                align: 'center'
            })
            .text('Jumlah (Rp.)', 393 + x, 196.5 + bodY, {
                width: 80,
                align: 'center'
            })
            // TAGIHAN SEBELUMNYA
            .font('letter_gothic_bold')
            .fontSize(8)
            .text('TAGIHAN SEBELUMNYA', 163, 220 + bodY, {
                width: 260,
                align: 'left'
            })
            // COUNTING PAGE
            .fontSize(6)
            .font('arial')
            .text(`${data.fh.halaman}`.padStart(6, '0'), 20, 635 + bodY);

        // INIT TEXT VALUE
        let rows = 240 + bodY;
        const max_line_per_page = 19;
        let rowCount = 0;
        const marginPlus = 389;
        const marginMinus = 403.5;
        // const max_per_page = (data.td.payments.length > 0 ? data.td.payments.length + 3 : 0) + (data.td.trans.length > 0 ? data.td.trans.length + 5 : 0); // set count line : max per 1 page is 19
        // console.log((data.td.payments.length > 0 ? data.td.payments.length + 3 : 0) + (data.td.trans.length > 0 ? data.td.trans.length + 5 : 0))
        // PAYMENTS LOOP
        if (data.td.payments.length !== 0){
            rowCount += 3;
            doc
                .fontSize(8)
                .font('letter_gothic_bold')
                .text('PAYMENTS', 163, rows)
            // PAYMENTS LOOP
            data.td.payments.forEach(element => {
                rowCount++;
                rows += 10
                doc
                    .font('letter_gothic')
                    .text(element.keterangan, 163, rows)
                    .text(element.tgl_transaksi, 40, rows)
                    .text(element.tgl_pembukuan, 105, rows)
                    .text(remove_plus_minus(element.jumlah), element.jumlah.includes('-') ? marginMinus : marginPlus, rows, {
                        width: 70,
                        align: 'right'
                    })
            })
            rows += 10
            doc
                .font('letter_gothic_bold')
                .text('TOTAL PAYMENTS', 163, rows)
                .text(remove_plus_minus(data.ah.pembayaran), data.ah.pembayaran.includes('-') ? marginMinus : marginPlus, rows, {
                    width: 70,
                    align: 'right'
                })
            rows += 20
        }
        // TRANS LOOP
        if (data.td.trans.length !== 0){
            rowCount += 5;
            // TRANS BODY
            doc
                .fontSize(8)
                .font('letter_gothic_bold')
                .text(card_strip_format(data.ah.card_no), 40, rows)
                .text(data.ah.name, 163, rows)

            data.td.trans.forEach(element => {
                rowCount++;
                rows += 10
                doc
                    .fontSize(8)
                    .font('letter_gothic')
                    .text(element.keterangan, 163, rows)
                    .text(element.tgl_transaksi, 40, rows)
                    .text(element.tgl_pembukuan, 105, rows)
                    .text(remove_plus_minus(element.jumlah), element.jumlah.includes('-') ? marginMinus : marginPlus, rows, {
                        width: 70,
                        align: 'right'
                    })
                // CHECK ROW COUNT
                if (rowCount > max_line_per_page){
                    data.ah.halaman++
                    this.extLayout(doc, data)
                    rowCount = 0
                    rows = 120
                }
            })
            rows += 20;
            // SUB TOTAL
            doc
                .font('letter_gothic_bold')
                .text(card_strip_format(data.ah.card_no), 40, rows)
                .text(data.ah.name, 163, rows)
                .text(remove_plus_minus(data.td.sub_total), data.td.sub_total.includes('-') ? marginMinus : marginPlus, rows, {
                    width: 70,
                    align: 'right'
                });
            // GRAND TOTAL
            rows += 20
            doc
                .text(card_strip_format(data.ah.card_no), 40, rows)
                .text(data.ah.name, 163, rows)
                .text(remove_plus_minus(data.td.sub_total), data.td.sub_total.includes('-') ? marginMinus : marginPlus, rows, {
                    width: 70,
                    align: 'right'
                });
        }
        // message AM
        doc
            .font('arial')
            .fontSize(7)
            .text(data.am.message.replace(/\s+/g, ' '), 120, 450 + bodY + temptRow, {
                width: 270,
                align: 'center'
            });
        // const h = doc.heightOfString(data.am.message.replace(/\s+/g, ' ')
        // console.log(h)
        // return
        // CLEARING
        // console.log(rowCount, max_line_per_page)
        rowCount = 0
    }
    // e.g REWARD, DETAIL TRANSACTION
    this.footer = (doc, data) => {
        const footY = this.gY;
        const mRewards = {
            bulan_lalu: 'MEGA Rewards bulan lalu',
            bulan_ini: 'MEGA Rewards bulan ini',
            yg_ditukarkan: 'MEGA Rewards yang ditukarkan',
            disesuaikan: 'MEGA Rewards disesuaikan',
            yg_tersedia: 'MEGA Rewards yang tersedia',
            akan_kedaluarsa: 'MEGA Rewards yang akan kadaluarsa bulan depan',
            bunga_pem: 'Bunga Pembelanjaan (%)',
            bunga_pen_tun: 'Bunga Penarikan Tunai (%)',
            bunga_trf_saldo: 'Bunga Transfer Saldo (%)',
            sisa_cicilan: 'Total Sisa Cicilan yang belum tertagih',
        }
        //region DESIGN
        doc
            // TITLE RECTANGLE
            .strokeColor('black')
            .lineWidth(1.5)
            // FOOTER 1
            .roundedRect(28, 490 + footY, 88.5, 30, 5)
            .roundedRect(141.5, 490 + footY, 88.5, 30, 5)
            .roundedRect(254.5, 490 + footY, 107.5, 30, 5)
            .roundedRect(385.5, 490 + footY, 88.5, 30, 5)
            // FOOTER 2
            .roundedRect(28, 524 + footY, 88.5, 30, 5)
            .roundedRect(141.5, 524 + footY, 88.5, 30, 5)
            .roundedRect(254.5, 524 + footY, 107.5, 30, 5)
            .roundedRect(385.5, 524 + footY, 88.5, 30, 5)
            // FOOTER 3
            .rect(28, 560 + footY, 221, 70)
            .rect(256, 560 + footY, 221, 70)
            .stroke();

        const fill = '#d2d3d5'
        doc
            // FOOTER 1
            .roundedRect(28.8, 490.5 + footY, 87, 8, 4.5)
            .roundedRect(142.3, 490.5 + footY, 87, 8, 4.5)
            .roundedRect(255.3, 490.5 + footY, 106, 8, 4.5)
            .roundedRect(386.3, 490.5 + footY, 87, 8, 4.5)
            .rect(28.8, 495 + footY, 87, 8)
            .rect(142.3, 495 + footY, 87, 8)
            .rect(255.3, 495 + footY, 106, 8)
            .rect(386.3, 495 + footY, 87, 8)
            // FOOTER 2
            .roundedRect(28.8, 524.5 + footY, 87, 8, 4.5)
            .roundedRect(142.3, 524.5 + footY, 87, 8, 4.5)
            .roundedRect(255.3, 524.5 + footY, 106, 8, 4.5)
            .roundedRect(386.3, 524.5 + footY, 87, 8, 4.5)
            .rect(28.8, 529 + footY, 87, 8)
            .rect(142.3, 529 + footY, 87, 8)
            .rect(255.3, 529 + footY, 106, 8)
            .rect(386.3, 529 + footY, 87, 8)
            .fill(fill)
        //endregion
        doc
            .fillColor('black')
            .font('Times-Roman')
            .fontSize(7.5)
            // TITLE
            // + 5y
            // FOOTER 1
            .text('Tagihan Sebelumnya (Rp)', 28, 494 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Pembayaran (Rp)', 141.5, 494 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Transaksi, Bunga & Biaya (Rp)', 254.5, 494 + footY, {
                width: 106,
                align: 'center'
            })
            .text('Total Tagihan (Rp)', 385.5, 494 + footY, {
                width: 87,
                align: 'center'
            })
        // + 5y
        // FOOTER 2
        doc
            .text('Batas Kredit (Rp)', 28, 528 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Batas Tarik Tunai (Rp)', 141.5, 528 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Sisa Kredit (Rp)', 254.5, 528 + footY, {
                width: 106,
                align: 'center'
            })
            .text('Saldo Tarik Tunai (Rp)', 385.5, 528 + footY, {
                width: 87,
                align: 'center'
            })

        //region VALUE 1
        doc
            .font('letter_gothic')
            .fontSize(8)
            .text(remove_plus_minus(data.ah.tagihan_sebelumnya), 28, 508 + footY, {
                width: 87,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.pembayaran), 141.5, 508 + footY, {
                width: 87,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.bunga_biaya_trans), 254.5, 508 + footY, {
                width: 106,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.total_tagihan), 385.5, 508 + footY, {
                width: 87,
                align: 'center'
            })
        //endregion VALUE 1

        //region VALUE 2
        doc
            .font('letter_gothic')
            .fontSize(8)
            .text(data.ah.batas_credit, 28, 542 + footY, {
                width: 87,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.batas_tarik_tunai), 141.5, 542 + footY, {
                width: 87,
                align: 'center'
            })
            .text(data.ah.sisa_credit, 254.5, 542 + footY, {
                width: 106,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.saldo_tarik_tunai), 385.5, 542 + footY, {
                width: 87,
                align: 'center'
            });
        //endregion
        //region TEXT MEDIUM BOX LEFT e.g REWARDS
        doc
            .font('Times-Bold')
            .fontSize(7)
            .text('Informasi MEGA Rewards', 34, 567 + footY)
            .fontSize(7)
            .text(mRewards.bulan_lalu.slice(0, 4), 43, 576 + footY, {
                continued: true
            })
            .font('Times-Roman')
            .text(mRewards.bulan_lalu.slice(4))
            .text('128,604', 192.5, 576 + footY, {
                width: 45,
                align: 'right'
            })
            //
            .font('Times-Bold')
            .text(mRewards.bulan_ini.slice(0, 4), 43, 584.5 + footY, {
                continued: true
            })
            .font('Times-Roman')
            .text(mRewards.bulan_ini.slice(4))
            .text('0', 192.5, 584.5 + footY, {
                width: 45,
                align: 'right'
            })
            //
            .font('Times-Bold')
            .text(mRewards.yg_ditukarkan.slice(0, 4), 43, 593.5 + footY, {
                continued: true
            })
            .font('Times-Roman')
            .text(mRewards.yg_ditukarkan.slice(4))
            .text('0', 192.5, 593.5 + footY, {
                width: 45,
                align: 'right'
            })
            //
            .font('Times-Bold')
            .text(mRewards.disesuaikan.slice(0, 4), 43, 602.5 + footY, {
                continued: true
            })
            .font('Times-Roman')
            .text(mRewards.disesuaikan.slice(4))
            .text('0', 192.5, 602.5 + footY, {
                width: 45,
                align: 'right'
            })
            //
            .font('Times-Bold')
            .text(mRewards.yg_tersedia.slice(0, 4), 43, 611.5 + footY, {
                continued: true
            })
            .font('Times-Roman')
            .text(mRewards.yg_tersedia.slice(4))
            .text('128,604', 192.5, 611.5 + footY, {
                width: 45,
                align: 'right'
            })
            //
            .font('Times-Bold')
            .text(mRewards.akan_kedaluarsa.slice(0, 4), 43, 620.5 + footY, {
                continued: true
            })
            .font('Times-Roman')
            .text(mRewards.akan_kedaluarsa.slice(4))
            .text('3,920', 192.5, 620.5 + footY, {
                width: 45,
                align: 'right'
            });
        //region TEXT MEDIUM BOX LEFT e.g REWARDS
        doc
            .font('Times-Roman')
            .fontSize(7)
            .text(mRewards.bunga_pem, 264, 567 + footY)
            .text('2.00/bln. 24.00/thn', 337, 567 + footY, {
                width: 125,
                align: 'right'
            })
            //
            .text(mRewards.bunga_pen_tun, 264, 584 + footY)
            .text('2.00/bln. 24.00/thn', 337, 584 + footY, {
                width: 125,
                align: 'right'
            })
            //
            .text(mRewards.bunga_trf_saldo, 264, 601 + footY)
            .text('0.00/bln. 0.00/thn', 337, 601 + footY, {
                width: 125,
                align: 'right'
            })
            //
            .text(mRewards.sisa_cicilan, 264, 618 + footY)
            .text('0', 337, 618 + footY, {
                width: 125,
                align: 'right'
            })
    }
    // FOOTER CASHLINE
    this.footer_cashline = (doc, data) => {
        const footY = this.gY;
        const mRewards = {
            bulan_lalu: 'MEGA Rewards bulan lalu',
            bulan_ini: 'MEGA Rewards bulan ini',
            yg_ditukarkan: 'MEGA Rewards yang ditukarkan',
            disesuaikan: 'MEGA Rewards disesuaikan',
            yg_tersedia: 'MEGA Rewards yang tersedia',
            akan_kedaluarsa: 'MEGA Rewards yang akan kadaluarsa bulan depan',
            bunga_pem: 'Bunga Pembelanjaan (%)',
            bunga_pen_tun: 'Bunga Penarikan Tunai (%)',
            bunga_trf_saldo: 'Bunga Transfer Saldo (%)',
            sisa_cicilan: 'Total Sisa Cicilan yang belum tertagih',
        }
        //region DESIGN
        doc
            // TITLE RECTANGLE
            .strokeColor('black')
            .lineWidth(1)
            // FOOTER 1
            // .roundedRect(28, 490 + footY, 88.5, 30, 5)
            // .roundedRect(141.5, 490 + footY, 88.5, 30, 5)
            // .roundedRect(254.5, 490 + footY, 107.5, 30, 5)
            // .roundedRect(385.5, 490 + footY, 88.5, 30, 5)
            // FOOTER 2
            // .roundedRect(28, 524 + footY, 88.5, 30, 5)
            // .roundedRect(141.5, 524 + footY, 88.5, 30, 5)
            // .roundedRect(254.5, 524 + footY, 107.5, 30, 5)
            // .roundedRect(385.5, 524 + footY, 88.5, 30, 5)
            // FOOTER 3
            .rect(28, 605 + footY, 450, 25)
            .font('Times-Roman')
            .fontSize(7.5)
            .text(`Bunga Penarikan Tunai ${data.ah.bunga_pembelanjaan} % /hari`, 28, 605 + footY, {
                width: 450,
                align: 'center'
            })
            // .rect(256, 560 + footY, 221, 70)
            .stroke();

        const fill = '#d2d3d5';
        doc
            // FOOTER 1
            .roundedRect(28.8, 490.5 + footY, 87, 8, 4.5)
            .roundedRect(142.3, 490.5 + footY, 87, 8, 4.5)
            .roundedRect(255.3, 490.5 + footY, 106, 8, 4.5)
            .roundedRect(386.3, 490.5 + footY, 87, 8, 4.5)
            .rect(28.8, 495 + footY, 87, 8)
            .rect(142.3, 495 + footY, 87, 8)
            .rect(255.3, 495 + footY, 106, 8)
            .rect(386.3, 495 + footY, 87, 8)
            // FOOTER 2
            .roundedRect(28.8, 524.5 + footY, 87, 8, 4.5)
            .roundedRect(142.3, 524.5 + footY, 87, 8, 4.5)
            .roundedRect(255.3, 524.5 + footY, 106, 8, 4.5)
            .roundedRect(386.3, 524.5 + footY, 87, 8, 4.5)
            .rect(28.8, 529 + footY, 87, 8)
            .rect(142.3, 529 + footY, 87, 8)
            .rect(255.3, 529 + footY, 106, 8)
            .rect(386.3, 529 + footY, 87, 8)
            .fill(fill);
        //endregion
        doc
            .fillColor('black')
            .font('Times-Roman')
            .fontSize(7.5)
            // TITLE
            // + 5y
            // FOOTER 1
            .text('Tagihan Sebelumnya (Rp)', 28, 494 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Pembayaran (Rp)', 141.5, 494 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Transaksi, Bunga & Biaya (Rp)', 254.5, 494 + footY, {
                width: 106,
                align: 'center'
            })
            .text('Total Tagihan (Rp)', 385.5, 494 + footY, {
                width: 87,
                align: 'center'
            })
        // + 5y
        // FOOTER 2
        doc
            .text('Batas Kredit (Rp)', 28, 528 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Batas Tarik Tunai (Rp)', 141.5, 528 + footY, {
                width: 87,
                align: 'center'
            })
            .text('Sisa Kredit (Rp)', 254.5, 528 + footY, {
                width: 106,
                align: 'center'
            })
            .text('Saldo Tarik Tunai (Rp)', 385.5, 528 + footY, {
                width: 87,
                align: 'center'
            })

        //region VALUE 1
        doc
            .font('letter_gothic')
            .fontSize(8)
            .text(remove_plus_minus(data.ah.tagihan_sebelumnya), 28, 508 + footY, {
                width: 87,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.pembayaran), 141.5, 508 + footY, {
                width: 87,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.bunga_biaya_trans), 254.5, 508 + footY, {
                width: 106,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.total_tagihan), 385.5, 508 + footY, {
                width: 87,
                align: 'center'
            })
        //endregion VALUE 1

        //region VALUE 2
        doc
            .font('letter_gothic')
            .fontSize(8)
            .text(data.ah.batas_credit, 28, 542 + footY, {
                width: 87,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.batas_tarik_tunai), 141.5, 542 + footY, {
                width: 87,
                align: 'center'
            })
            .text(data.ah.sisa_credit, 254.5, 542 + footY, {
                width: 106,
                align: 'center'
            })
            .text(remove_plus_minus(data.ah.saldo_tarik_tunai), 385.5, 542 + footY, {
                width: 87,
                align: 'center'
            });
        //endregion
        // //region TEXT MEDIUM BOX LEFT e.g REWARDS
        // doc
        //     .font('Times-Bold')
        //     .fontSize(7)
        //     .text('Informasi MEGA Rewards', 34, 567 + footY)
        //     .fontSize(7)
        //     .text(mRewards.bulan_lalu.slice(0, 4), 43, 576 + footY, {
        //         continued: true
        //     })
        //     .font('Times-Roman')
        //     .text(mRewards.bulan_lalu.slice(4))
        //     .text('128,604', 192.5, 576 + footY, {
        //         width: 45,
        //         align: 'right'
        //     })
        //     //
        //     .font('Times-Bold')
        //     .text(mRewards.bulan_ini.slice(0, 4), 43, 584.5 + footY, {
        //         continued: true
        //     })
        //     .font('Times-Roman')
        //     .text(mRewards.bulan_ini.slice(4))
        //     .text('0', 192.5, 584.5 + footY, {
        //         width: 45,
        //         align: 'right'
        //     })
        //     //
        //     .font('Times-Bold')
        //     .text(mRewards.yg_ditukarkan.slice(0, 4), 43, 593.5 + footY, {
        //         continued: true
        //     })
        //     .font('Times-Roman')
        //     .text(mRewards.yg_ditukarkan.slice(4))
        //     .text('0', 192.5, 593.5 + footY, {
        //         width: 45,
        //         align: 'right'
        //     })
        //     //
        //     .font('Times-Bold')
        //     .text(mRewards.disesuaikan.slice(0, 4), 43, 602.5 + footY, {
        //         continued: true
        //     })
        //     .font('Times-Roman')
        //     .text(mRewards.disesuaikan.slice(4))
        //     .text('0', 192.5, 602.5 + footY, {
        //         width: 45,
        //         align: 'right'
        //     })
        //     //
        //     .font('Times-Bold')
        //     .text(mRewards.yg_tersedia.slice(0, 4), 43, 611.5 + footY, {
        //         continued: true
        //     })
        //     .font('Times-Roman')
        //     .text(mRewards.yg_tersedia.slice(4))
        //     .text('128,604', 192.5, 611.5 + footY, {
        //         width: 45,
        //         align: 'right'
        //     })
        //     //
        //     .font('Times-Bold')
        //     .text(mRewards.akan_kedaluarsa.slice(0, 4), 43, 620.5 + footY, {
        //         continued: true
        //     })
        //     .font('Times-Roman')
        //     .text(mRewards.akan_kedaluarsa.slice(4))
        //     .text('3,920', 192.5, 620.5 + footY, {
        //         width: 45,
        //         align: 'right'
        //     });
        // //region TEXT MEDIUM BOX LEFT e.g REWARDS
        // doc
        //     .font('Times-Roman')
        //     .fontSize(7)
        //     .text(mRewards.bunga_pem, 264, 567 + footY)
        //     .text('2.00/bln. 24.00/thn', 337, 567 + footY, {
        //         width: 125,
        //         align: 'right'
        //     })
        //     //
        //     .text(mRewards.bunga_pen_tun, 264, 584 + footY)
        //     .text('2.00/bln. 24.00/thn', 337, 584 + footY, {
        //         width: 125,
        //         align: 'right'
        //     })
        //     //
        //     .text(mRewards.bunga_trf_saldo, 264, 601 + footY)
        //     .text('0.00/bln. 0.00/thn', 337, 601 + footY, {
        //         width: 125,
        //         align: 'right'
        //     })
        //     //
        //     .text(mRewards.sisa_cicilan, 264, 618 + footY)
        //     .text('0', 337, 618 + footY, {
        //         width: 125,
        //         align: 'right'
        //     })
    }
    // e.g PAGE MORE THAN 1
    this.extLayout = (doc, data) => {
        doc.addPage()
        // INIT BODY DESIGN
        // const y = 0;
        const fill = '#d2d3d5';
        data.fh.halaman++
        // HEADER
        const xySetter = {
            // HEADER
            hx: -15,
            hy: this.gY,
            // BIG RECT HEADER
            brhx: 4,
            brhy: -90 + this.gY

        }
        doc
            // RECT
            .roundedRect(35 + xySetter.hx, 55 + xySetter.hy, 105, 30, 5)
            .stroke()
            // MINI ROUNDED
            .roundedRect(35.5 + xySetter.hx, 55.5 + xySetter.hy, 104, 8, 4.5)
            .rect(35.5 + xySetter.hx, 60 + xySetter.hy, 104, 8)
            // MINI RECT
            .fill(fill)
            // TITLE
            .fillColor('black')
            .font('Times-Roman')
            .fontSize(7)
            .text('Nomor Kartu Anda', 35 + xySetter.hx, 60 + xySetter.hy, {
                width: 105,
                align: 'center'
            })
            // VALUE
            .font('letter_gothic')
            .fontSize(8)
            // NOMER KARTU ANDA
            .text(card_strip_format(data.ah.card_no), 35 + xySetter.hx, 72.5 + xySetter.hy, {
                width: 105,
                align: 'center'
            })
            // CODE KURIR
            .font('arial')
            .fontSize(6)
            .text('004571 / NS_T2_000', 20, 90 + xySetter.hy)
        // END HEADER
        // ('x', 'x_to', 'y', 'y_to')
        // BIG RECTANGLE & SUB HEADER
        doc
            // BIG RECTANGLE
            .rect(20, 98 + xySetter.hy, 465, 535) // 445
            .lineWidth(1.5)
            .stroke()
            // SUB HEADER
            .roundedRect(25 + xySetter.brhx, 192.5 + xySetter.brhy, 60, 15, 5)
            .roundedRect(90 + xySetter.brhx, 192.5  + xySetter.brhy, 60, 15, 5)
            .roundedRect(155 + xySetter.brhx, 192.5 + xySetter.brhy, 233, 15, 5)
            .roundedRect(393 + xySetter.brhx, 192.5 + xySetter.brhy, 80, 15, 5)
            .fillAndStroke(fill);
        doc
            .font('arial')
            .fontSize(10)
            .fillColor('black')
            .text('Lembar Penagihan', 205 + xySetter.brhx, 175 + xySetter.brhy)
            .font('Times-Roman')
            .fontSize(8)
            .text('Tgl. Transaksi', 25 + xySetter.brhx, 196.5 + xySetter.brhy, {
                width: 60,
                align: 'center'
            })
            .text('Tgl. Pembukuan', 90 + xySetter.brhx, 196.5 + xySetter.brhy, {
                width: 60,
                align: 'center'
            })
            .text('Keterangan', 155 + xySetter.brhx, 196.5 + xySetter.brhy, {
                width: 233,
                align: 'center'
            })
            .text('Jumlah (Rp.)', 393 + xySetter.brhx, 196.5 + xySetter.brhy, {
                width: 80,
                align: 'center'
            })
            // HALAMAN EVERY USER
            .font('letter_gothic')
            .fontSize(6)
            .text(`Halaman : ${data.ah.halaman < 9 ? '0'+data.ah.halaman : data.ah.halaman}`, 437 + xySetter.brhx, 180 + xySetter.brhy)
            // COUNTING GLOBAL PAGE
            .fontSize(6)
            .font('arial')
            .text(`${data.fh.halaman}`.padStart(6, '0'), 20, 635 + xySetter.hy);

    }
    //
    this.initFont = (doc) => {
        doc.registerFont('arial', 'public/fonts/bank-mega/arial_0.ttf');
        doc.registerFont('arial_bold', 'public/fonts/bank-mega/arialbd_0.ttf');
        doc.registerFont('letter_gothic', 'public/fonts/bank-mega/letter_gothic.ttf');
        doc.registerFont('letter_gothic_bold', 'public/fonts/bank-mega/letter_gothic_bold.ttf');
        doc.registerFont('code_3_of_9', 'public/fonts/bank-mega/Code3of9_0.ttf');
    };
}

function FUNC(){
    this.calc_bunga_bulan = (value) => {
        return `${(value / 12).toFixed(2)}/bln. ${value}/thn`;
    }
    this.calc_trf_saldo = (value) => {
        return `${value.val1}/bln. ${value.val2}/thn`;
    }
}

function INIT() {
    // GENERATE EXCEL TO JSON LIST JANGAN CETAK BY CARD NO
    this.jangan_di_cetak = async (path) => {
        const data =  excelToJson({
            sourceFile: path,
            // sheets:[{
            //     name: 'card_no',
            // }],
            columnToKey: {
                A: 'NO_KARTU',
                // B: 'NAMA',
                // C: 'ALAMAT',
                // D: 'NO_KARTU',
                // E: 'CYCLE',
                // F: 'REQUEST',
            },
            // header:{
            //     // Is the number of rows that will be skipped and will not be present at our result object. Counting from top to bottom
            //     rows: 3 // 2, 3, 4, etc.
            // }
        })
        return data[Object.keys(data)[0]]
    };
    // GENERATE EXCEL LIST KUNCIAN KURIR
    this.kuncian_kurir = async (path) => {
        const data =  excelToJson({
            sourceFile: path,
            // sheets:[{
            //     name: 'card_no',
            // }],
            columnToKey: {
                A: 'NO',
                B: 'NAMA',
                C: 'NO_KARTU',
                D: 'CYC',
                E: 'DIALIHKAN',
                F: 'KOTA',
            },
            header:{
                // Is the number of rows that will be skipped and will not be present at our result object. Counting from top to bottom
                rows: 1 // 2, 3, 4, etc.
            }
        })
        return data[Object.keys(data)[0]]
    };
    //
    this.lempar = (path) => {
        const data = excelToJson({
            sourceFile: path,
            // sheets:[{
            //     name: 'card_no',
            // }],
            columnToKey: {
                A: 'NO',
                B: 'NAMA',
                C: 'ALAMAT',
                D: 'NO_KARTU',
                E: 'CYCLE',
                F: 'REQUEST',
            },
            header:{
                // Is the number of rows that will be skipped and will not be present at our result object. Counting from top to bottom
                rows: 1 // 2, 3, 4, etc.
            }
        })
        return data[Object.keys(data)[0]]
    };
    //
    this.pindah_kurir = (path) => {
        const data = excelToJson({
            sourceFile: path,
            // sheets:[{
            //     name: 'card_no',
            // }],
            columnToKey: {
                A: 'NO',
                B: 'NAMA',
                C: 'NO_KARTU',
                D: 'CYCLE',
                E: 'DIALIHKAN_KE',
                F: 'KOTA',
            },
            header:{
                // Is the number of rows that will be skipped and will not be present at our result object. Counting from top to bottom
                rows: 1 // 2, 3, 4, etc.
            }
        })
        return data[Object.keys(data)[0]]
    };
    //
}

function EXCEL() {
    // this.cntRow = 4;
    this.periode = null;
    this.det_rekap = [];
    this.initExcel = (periode) => {
        this.excel = [
            {
                name: 'CETAK',
                workbook: new ExcelJS.Workbook(),
                pathFolder: outputPath + periode + '/LOG_NASABAH_CETAK/',
                pathFile: ''
            },
            {
                name: 'HOLD',
                workbook: new ExcelJS.Workbook(),
                pathFolder: outputPath + periode + '/LOG_NASABAH_HOLD_(TIDAK_CETAK)/',
                pathFile: ''
            },
            {
                name: 'LISTING KOTA',
                workbook: new ExcelJS.Workbook(),
                pathFolder: outputPath + periode + '/LISTING_KOTA/',
                pathFile: ''
            },
            // {
            //     name: 'REKAP',
            //     workbook: new ExcelJS.Workbook(),
            //     pathFolder: outputPath + periode + '/',
            //     pathFile: ''
            // },
        ];
        this.rekap_excel = [
            {
                name: 'REKAP',
                workbook: new ExcelJS.Workbook(),
                pathFolder: outputPath + periode + '/',
                pathFile: ''
            },
            {
                name: 'REKAP PRODUKSI',
                workbook: new ExcelJS.Workbook(),
                pathFolder: outputPath + periode + '/',
                pathFile: ''
            },
        ];
        // INIT WORKBOOK REKAP EXCEL
        this.rekap_excel.forEach(el => {
            el.worksheet = el.workbook.addWorksheet(el.name);
        });
        // INIT WORKBOOK DEPENDENCIES
        this.excel.forEach(el => {
            el.worksheet = el.workbook.addWorksheet(el.name);
        })
    }
    // INIT LOG CETAK
    this.cetakLogExcel = (num, data, index_all_kurir) => {
        const excl = this.excel.find(x => x.name === 'CETAK')
        excl.workbook.creator = 'DKR With Nin';
        excl.workbook.lastModifiedBy = 'Anony';
        excl.workbook.created = new Date(1985, 8, 30);
        excl.workbook.modified = new Date();
        excl.workbook.lastPrinted = new Date(2016, 9, 27);
        excl.workbook.properties.date1904 = true;
        // this for opened the xlsx
        excl.workbook.views = [
            {
                x: 0, y: 0, width: 20000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ]
        // // create worksheet
        // let worksheet = this.workbook.addWorksheet('My Sheet');
        //   worksheet.getCell('A1').dataValidation = {
        //     type: 'whole',
        //     operator: 'notEqual',
        //     showErrorMessage: true,
        //     formulae: [5],
        //     errorStyle: 'error',
        //     errorTitle: 'Five',
        //     error: 'The value must not be Five'
        // };

        excl.worksheet.columns = [
            {header: 'SEQNO', key: 'seqno', width: 8},
            {header: 'REFFILE', key: 'reffile', width: 8},
            {header: 'CC', key: 'cc', width: 32},
            {header: 'NAMA', key: 'nama', width: 32},
            {header: 'COMPANY', key: 'company', width: 32},
            {header: 'ALAMAT1', key: 'alamat1', width: 32},
            {header: 'ALAMAT2', key: 'alamat2', width: 32},
            {header: 'ALAMAT3', key: 'alamat3', width: 32},
            {header: 'ALAMAT4', key: 'alamat4', width: 32},
            {header: 'ALAMAT5', key: 'alamat5', width: 32},
            {header: 'KOTA', key: 'kota', width: 32},
            {header: 'KDPOS', key: 'kdpos', width: 32},
            {header: 'NFILE', key: 'nfile', width: 10, outlineLevel: 1},
            {header: 'PAGES', key: 'pages', width: 15},
            {header: 'BARCODE', key: 'barcode', width: 15},
            {header: 'REQUEST', key: 'request', width: 15},
            {header: 'C_KURIR', key: 'c_kurir', width: 15},
            {header: 'KURIR', key: 'kurir', width: 15},
            {header: 'NO_KURIR', key: 'no_kurir', width: 15},
            {header: 'B1', key: 'b1', width: 15},
            {header: 'B2', key: 'b2', width: 15},
            {header: 'B3', key: 'b3', width: 15},
            {header: 'JNS', key: 'jns', width: 15},
            {header: 'JNS_DET', key: 'jns_det', width: 15},
            {header: 'WAYB', key: 'wayb', width: 15},
            {header: 'FILENAME', key: 'filename', width: 15},
            {header: 'NO_FILE', key: 'no_file', width: 15},
            {header: 'STFILE', key: 'stfile', width: 15},
        ];

        // data.forEach((element, i) => {
        excl.worksheet.addRow(
            {
                seqno: num.toString().padStart(6, '0'), //data.fh.seq_number_user.toString().padStart(6, '0'),
                reffile: data.fh.product_code,
                cc: data.ah.card_no,
                nama: data.ah.name,
                company: data.ah.company_name,
                alamat1: data.ah.addr1,
                alamat2: data.ah.addr2,
                alamat3: data.ah.addr3,
                alamat4: data.ah.addr4,
                alamat5: data.ah.addr5,
                kota: data.ah.addr5,
                kdpos: data.ah.pos_code,
                nfile: data.fh.kurir.nfile,
                pages: data.ah.halaman,
                barcode: data.ah.barcode_full,
                request: data.ah.request,
                c_kurir: data.fh.kurir.fullname,
                kurir: data.fh.kurir.name,
                no_kurir: index_all_kurir,
                b1: '0',
                b2: '0',
                b3: '0',
                jns: data.fh.detail.jenis,
                jns_det: data.fh.detail.name.toUpperCase(),
                wayb: '0',
                filename: data.fh.kurir.name + '_' + data.fh.detail.jenis + data.fh.listKurir.find(x => x.fullname === data.fh.kurir.fullname).id+'_000',
                no_file: '0',
                stfile: data.fh.stmtfile,
            });
        // console.log(element.merchant)
        // console.log(i)
        // });
    };
    // INIT LOG HOLD
    this.cetakLogHold = (num, data) => {
        const excl = this.excel.find(x => x.name === 'HOLD')
        excl.workbook.creator = 'DKR With Nin';
        excl.workbook.lastModifiedBy = 'Anony';
        excl.workbook.created = new Date(1985, 8, 30);
        excl.workbook.modified = new Date();
        excl.workbook.lastPrinted = new Date(2016, 9, 27);
        excl.workbook.properties.date1904 = true;
        // this for opened the xlsx
        excl.workbook.views = [
            {
                x: 0, y: 0, width: 20000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ]
        // // create worksheet
        // let worksheet = this.workbook.addWorksheet('My Sheet');
        //   worksheet.getCell('A1').dataValidation = {
        //     type: 'whole',
        //     operator: 'notEqual',
        //     showErrorMessage: true,
        //     formulae: [5],
        //     errorStyle: 'error',
        //     errorTitle: 'Five',
        //     error: 'The value must not be Five'
        // };

        excl.worksheet.columns = [
            {header: 'SEQNO', key: 'seqno', width: 8},
            {header: 'REFFILE', key: 'reffile', width: 8},
            {header: 'CC', key: 'cc', width: 32},
            {header: 'NAMA', key: 'nama', width: 32},
            {header: 'COMPANY', key: 'company', width: 32},
            {header: 'ALAMAT1', key: 'alamat1', width: 32},
            {header: 'ALAMAT2', key: 'alamat2', width: 32},
            {header: 'ALAMAT3', key: 'alamat3', width: 32},
            {header: 'ALAMAT4', key: 'alamat4', width: 32},
            {header: 'ALAMAT5', key: 'alamat5', width: 32},
            {header: 'KOTA', key: 'kota', width: 32},
            {header: 'KDPOS', key: 'kdpos', width: 32},
            {header: 'NFILE', key: 'nfile', width: 10, outlineLevel: 1},
            {header: 'PAGES', key: 'pages', width: 15},
            {header: 'BARCODE', key: 'barcode', width: 15},
            {header: 'REQUEST', key: 'request', width: 15},
            {header: 'C_KURIR', key: 'c_kurir', width: 15},
            {header: 'KURIR', key: 'kurir', width: 15},
            {header: 'NO_KURIR', key: 'no_kurir', width: 15},
            {header: 'B1', key: 'b1', width: 15},
            {header: 'B2', key: 'b2', width: 15},
            {header: 'B3', key: 'b3', width: 15},
            {header: 'JNS', key: 'jns', width: 15},
            {header: 'JNS_DET', key: 'jns_det', width: 15},
            {header: 'WAYB', key: 'wayb', width: 15},
            {header: 'FILENAME', key: 'filename', width: 15},
            {header: 'NO_FILE', key: 'no_file', width: 15},
            {header: 'STFILE', key: 'stfile', width: 15},
        ];

        // data.forEach((element, i) => {
        excl.worksheet.addRow(
            {
                seqno: num.toString().padStart(6, '0'), // data.fh.seq_number_user.toString().padStart(6, '0'),
                reffile: data.fh.product_code,
                cc: data.ah.card_no,
                nama: data.ah.name,
                company: data.ah.company_name,
                alamat1: data.ah.addr1,
                alamat2: data.ah.addr2,
                alamat3: data.ah.addr3,
                alamat4: data.ah.addr4,
                alamat5: data.ah.addr5,
                kota: data.ah.addr5,
                kdpos: data.ah.pos_code,
                nfile: data.fh.kurir.nfile,
                pages: data.ah.halaman,
                barcode: data.ah.barcode_full,
                request: 'HOLD',
                c_kurir: data.fh.kurir.fullname,
                kurir: data.fh.kurir.name,
                no_kurir: '0',
                b1: '0',
                b2: '0',
                b3: '0',
                jns: data.fh.detail.jenis,
                jns_det: data.fh.detail.name.toUpperCase(),
                wayb: '0',
                filename: data.fh.kurir.name + '_' + data.fh.detail.jenis+data.fh.seq_stmtfile+'_000',
                no_file: '0',
                stfile: data.fh.stmtfile,
            });
        // console.log(element.merchant)
        // console.log(i)
        // });
    };
    // INIT LOG LISTING KOTA
    this.listing_kota = (data) => {
        const excl = this.excel.find(x => x.name === 'LISTING KOTA')
        excl.workbook.creator = 'DKR With Nin';
        excl.workbook.lastModifiedBy = 'Anony';
        excl.workbook.created = new Date(1985, 8, 30);
        excl.workbook.modified = new Date();
        excl.workbook.lastPrinted = new Date(2016, 9, 27);
        excl.workbook.properties.date1904 = true;
        // this for opened the xlsx
        excl.workbook.views = [
            {
                x: 0, y: 0, width: 20000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ]
        // // create worksheet
        // let worksheet = this.workbook.addWorksheet('My Sheet');
        //   worksheet.getCell('A1').dataValidation = {
        //     type: 'whole',
        //     operator: 'notEqual',
        //     showErrorMessage: true,
        //     formulae: [5],
        //     errorStyle: 'error',
        //     errorTitle: 'Five',
        //     error: 'The value must not be Five'
        // };

        excl.worksheet.columns = [
            {header: 'KOTA', key: 'kota', width: 8},
            {header: 'JUMLAH', key: 'jumlah', width: 8},
            {header: 'AGENT', key: 'agent', width: 32},
        ];

        data.forEach((el, i) => {
            excl.worksheet.addRow(
                {
                    seqno: i, //data.fh.seq_number_user.toString().padStart(6, '0'),
                    kota: el.kota,
                    jumlah: el.jumlah,
                    agent: el.agent,
                });
        })
        excl.worksheet.addRow(
            {
                kota: 'GRAND TOTAL',
                jumlah: data.reduce(function (acc, obj) { return acc + obj.jumlah; }, 0),
            })
        // listingKota.reduce(function (acc, obj) { return acc + obj.jumlah; }, 0)
    }
    // STOP CETAK ALL EXCEL
    this.stopCetak = async () => {
        // const list = this.excel.filter(x => x.name !== 'REKAP')
        for (const el of this.excel) {
            if (!fs.existsSync(el.pathFolder)) {
                console.log('file not exist and will be created...');
                const made = mkdirp.sync(el.pathFolder);
                console.log(`made directories, starting with ${made}`);
                console.log('done create directory');
            }
            await el.workbook.xlsx.writeFile(el.pathFolder + el.pathFile)
        }
        console.log('done excel')
    }
    // REKAP PRODUCT
    this.rekap = async () => {
        const excl = this.rekap_excel.find(x => x.name === 'REKAP')
        excl.workbook.creator = 'DKR With Nin';
        excl.workbook.lastModifiedBy = 'Anony';
        excl.workbook.created = new Date(1985, 8, 30);
        excl.workbook.modified = new Date();
        excl.workbook.lastPrinted = new Date(2016, 9, 27);
        excl.workbook.properties.date1904 = true;
        // this for opened the xlsx
        excl.workbook.views = [
            {
                x: 0, y: 0, width: 20000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ]
        // create worksheet
        //   worksheet.getCell('A1').dataValidation = {
        //     type: 'whole',
        //     operator: 'notEqual',
        //     showErrorMessage: true,
        //     formulae: [5],
        //     errorStyle: 'error',
        //     errorTitle: 'Five',
        //     error: 'The value must not be Five'
        // };
        // add a table to a sheet
        // STYLE
        const totalFill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {argb: '00FFFF'}
        };
        const headerFill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {argb: 'C0C0C0'}
        };
        const headerName = ['Kurir', 'Kode Kurir', 'Account', 'Pages', 'Sequence Kurir No', 'Sequence Kurir No']
        let cntRow = 1;
        excl.worksheet.getCell(cntRow, 1).value = 'Rekap Bank Mega';
        excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}};
        cntRow += 1;
        excl.worksheet.getCell(cntRow, 1).value = `Cyc ${moment(this.periode).format('DD MMMM YYYY').toUpperCase()}`;
        excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}};
        cntRow += 2;
        this.det_rekap.forEach((el, idx) => {
            excl.worksheet.getCell(cntRow, 1).value = el.prod.toUpperCase();
            excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}}
            cntRow++;
            excl.worksheet.getRow(cntRow).values = ['kurir', 'code', 'acc', 'pages', 'seq_s'];
            excl.worksheet.mergeCells(`E${cntRow}:F${cntRow}`)
            const row = excl.worksheet.getRow(cntRow);
            row.eachCell({includeEmpty: true}, function (cell, colNumber) {
                // console.log('Cell ' + colNumber + ' = ' + cell.value);
                // console.log(data[idx].kur.length)
                cell.value = headerName[colNumber - 1];
                cell.style = {font: {bold: true, name: 'Arial'}};
                cell.alignment = {vertical: 'top', horizontal: 'center'};
                // cell.style = { font: { name: 'Arial', size: 15 } }
                cell.fill = headerFill;
                cell.border = {
                    //     // top: {style:'thin'},
                    //     // left: {style:'thin'},
                    bottom: {style: 'medium'},
                    //     // right: {style:'thin'}
                }
            });
            excl.worksheet.columns = [
                {key: 'kurir', width: 8},
                {key: 'code', width: 15},
                {key: 'acc', width: 8},
                {key: 'pages', width: 8},
                {key: 'seq_s', width: 8},
                {key: 'seq_e', width: 8},
            ];
            el.kur.forEach(el2 => {
                excl.worksheet.addRow(
                    {
                        kurir: el2.name,
                        code: el2.code,
                        acc: el2.account,
                        pages: el2.pages,
                        seq_s: el2.sequence.start,
                        seq_e: el2.sequence.end,
                    });
            })
            cntRow += el.kur.length + 1;
            // BOTTOM BORDER
            const row2 = excl.worksheet.getRow(cntRow - 1);
            row2.eachCell({includeEmpty: true}, function (cell, colNumber) {
                // console.log('Cell ' + colNumber + ' = ' + cell.value);
                cell.border = {
                    //     // top: {style:'thin'},
                    //     // left: {style:'thin'},
                    bottom: {style: 'medium'},
                    //     // right: {style:'thin'}
                }
            });
            // TEXT
            excl.worksheet.getCell(cntRow, 1).value = 'Total';
            excl.worksheet.getCell(cntRow, 3).value = el.total.account;
            excl.worksheet.getCell(cntRow, 4).value = el.total.pages;
            const row3 = excl.worksheet.getRow(cntRow);
            row3.eachCell({includeEmpty: true}, function (cell, colNumber) {
                cell.fill = totalFill
                cell.style = {font: {bold: true, name: 'Arial'}};
            });

            // FILL
            excl.worksheet.getCell(cntRow, 1).fill = totalFill;
            excl.worksheet.getCell(cntRow, 2).fill = totalFill;
            excl.worksheet.getCell(cntRow, 3).fill = totalFill;
            excl.worksheet.getCell(cntRow, 4).fill = totalFill;
            cntRow += 2;
        })

        const GrdTtlAcc = this.det_rekap.reduce(function (acc, obj) { return acc + obj.total.account; }, 0); // SET TOTAL ACCOUNT REKAP
        const GTPages = this.det_rekap.reduce(function (acc, obj) { return acc + obj.total.pages; }, 0); // SET TOTAL ACCOUNT REKAP
        // cntRow += 2;
        excl.worksheet.getCell(cntRow, 1).value = 'GRAND TOTAL';
        excl.worksheet.getCell(cntRow, 3).value = GrdTtlAcc;
        excl.worksheet.getCell(cntRow, 4).value = GTPages;
        excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}};
        const row3 = excl.worksheet.getRow(cntRow);
        row3.eachCell({includeEmpty: true}, function (cell, colNumber) {
            cell.style = {font: {bold: true, name: 'Arial', size: 13}};
            cell.fill = headerFill;
        });

        // END STOP
        // const rekap = this.excel.find(x => x.name === 'REKAP');
        if (!fs.existsSync(excl.pathFolder)) {
            console.log('file not exist and will be created...');
            const made = mkdirp.sync(excl.pathFolder);
            console.log(`made directories, starting with ${made}`);
            console.log('done create directory');
        }
        await excl.workbook.xlsx.writeFile(excl.pathFolder + excl.pathFile);
        console.log('Rekap Done')
    }
    // REKAP PRODUKSI
    this.rekap_produksi = async () => {
        const excl = this.rekap_excel.find(x => x.name === 'REKAP PRODUKSI')
        excl.workbook.creator = 'DKR With Nin';
        excl.workbook.lastModifiedBy = 'Anony';
        excl.workbook.created = new Date(1985, 8, 30);
        excl.workbook.modified = new Date();
        excl.workbook.lastPrinted = new Date(2016, 9, 27);
        excl.workbook.properties.date1904 = true;
        // this for opened the xlsx
        excl.workbook.views = [
            {
                x: 0, y: 0, width: 20000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ]
        // create worksheet
        //   worksheet.getCell('A1').dataValidation = {
        //     type: 'whole',
        //     operator: 'notEqual',
        //     showErrorMessage: true,
        //     formulae: [5],
        //     errorStyle: 'error',
        //     errorTitle: 'Five',
        //     error: 'The value must not be Five'
        // };
        // add a table to a sheet
        // STYLE
        const totalFill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {argb: '00FFFF'}
        };
        const headerFill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {argb: 'C0C0C0'}
        };
        const headerName = ['Kurir', 'Filename', 'Account', 'Pages']
        let cntRow = 1;
        excl.worksheet.getCell(cntRow, 1).value = 'Rekap Bank Mega';
        excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}};
        cntRow += 1;
        excl.worksheet.getCell(cntRow, 1).value = `Cyc ${moment(this.periode).format('DD MMMM YYYY').toUpperCase()}`;
        excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}};
        cntRow += 2;
        this.det_rekap.forEach((el, idx) => {
            excl.worksheet.getCell(cntRow, 1).value = el.prod.toUpperCase();
            excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}}
            cntRow++;
            excl.worksheet.getRow(cntRow).values = ['kurir', 'code', 'acc', 'pages'];
            // excl.worksheet.mergeCells(`E${cntRow}:F${cntRow}`)
            const row = excl.worksheet.getRow(cntRow);
            row.eachCell({includeEmpty: true}, function (cell, colNumber) {
                // console.log('Cell ' + colNumber + ' = ' + cell.value);
                // console.log(data[idx].kur.length)
                cell.value = headerName[colNumber - 1];
                cell.style = {font: {bold: true, name: 'Arial'}};
                cell.alignment = {vertical: 'top', horizontal: 'center'};
                // cell.style = { font: { name: 'Arial', size: 15 } }
                cell.fill = headerFill;
                cell.border = {
                    //     // top: {style:'thin'},
                    //     // left: {style:'thin'},
                    bottom: {style: 'medium'},
                    //     // right: {style:'thin'}
                }
            });
            excl.worksheet.columns = [
                {key: 'kurir', width: 8},
                {key: 'code', width: 15},
                {key: 'acc', width: 8},
                {key: 'pages', width: 8},
                // {key: 'seq_s', width: 8},
                // {key: 'seq_e', width: 8},
            ];
            el.kur.forEach(el2 => {
                excl.worksheet.addRow(
                    {
                        kurir: el2.name,
                        code: el2.filename,
                        acc: el2.account,
                        pages: el2.pages,
                        // seq_s: el2.sequence.start,
                        // seq_e: el2.sequence.end,
                    });
            })
            cntRow += el.kur.length + 1;
            // BOTTOM BORDER
            const row2 = excl.worksheet.getRow(cntRow - 1);
            row2.eachCell({includeEmpty: true}, function (cell, colNumber) {
                // console.log('Cell ' + colNumber + ' = ' + cell.value);
                cell.border = {
                    //     // top: {style:'thin'},
                    //     // left: {style:'thin'},
                    bottom: {style: 'medium'},
                    //     // right: {style:'thin'}
                }
            });
            // TEXT
            excl.worksheet.getCell(cntRow, 1).value = 'Total';
            excl.worksheet.getCell(cntRow, 3).value = el.total.account;
            excl.worksheet.getCell(cntRow, 4).value = el.total.pages;
            const row3 = excl.worksheet.getRow(cntRow);
            row3.eachCell({includeEmpty: true}, function (cell, colNumber) {
                cell.fill = totalFill
                cell.style = {font: {bold: true, name: 'Arial'}};
            });

            // FILL
            excl.worksheet.getCell(cntRow, 1).fill = totalFill;
            excl.worksheet.getCell(cntRow, 2).fill = totalFill;
            excl.worksheet.getCell(cntRow, 3).fill = totalFill;
            excl.worksheet.getCell(cntRow, 4).fill = totalFill;
            cntRow += 2;
        })

        const GrdTtlAcc = this.det_rekap.reduce(function (acc, obj) { return acc + obj.total.account; }, 0); // SET TOTAL ACCOUNT REKAP
        const GTPages = this.det_rekap.reduce(function (acc, obj) { return acc + obj.total.pages; }, 0); // SET TOTAL ACCOUNT REKAP
        // cntRow += 2;
        excl.worksheet.getCell(cntRow, 1).value = 'GRAND TOTAL';
        excl.worksheet.getCell(cntRow, 3).value = GrdTtlAcc;
        excl.worksheet.getCell(cntRow, 4).value = GTPages;
        excl.worksheet.getCell(cntRow, 1).style = {font: {name: 'Arial', size: 15}};
        const row3 = excl.worksheet.getRow(cntRow);
        row3.eachCell({includeEmpty: true}, function (cell, colNumber) {
            cell.style = {font: {bold: true, name: 'Arial', size: 13}};
            cell.fill = headerFill;
        });

        // END STOP
        // const rekap = this.excel.find(x => x.name === 'REKAP');
        if (!fs.existsSync(excl.pathFolder)) {
            console.log('file not exist and will be created...');
            const made = mkdirp.sync(excl.pathFolder);
            console.log(`made directories, starting with ${made}`);
            console.log('done create directory');
        }
        await excl.workbook.xlsx.writeFile(excl.pathFolder + excl.pathFile);
        this.det_rekap = [];
        console.log('Rekap Produksi Done')
    }
}

function SUMMARY() {
    this.summaryProduct = [];
    this.periode = null;
    this.parseSummary = async (filePath, product, periode) => {
        this.periode = periode;
        const fileStream = fs.createReadStream(filePath); // STMT FILE PATH
        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });
        const summary = {
            product: product.toUpperCase(),
            list: []
        }
        for await (const line of rl) {
            const data = {};
            if (line.substring(0, 4) === 'STMT') {
                data.filename = line.substring(0, 25).trim();
                data.total = line.substring(71, 81).trim();
                summary.list.push(data)
            }
            // console.log(line)
        }
        this.summaryProduct.push(summary);
    }
    this.cetakSummary = async () => {
        const logger = fs.createWriteStream(outputPath + this.periode + '/summary_stfile.TXT', {
            flags: 'a' // 'a' means appending (old data will be preserved)
        })

        this.summaryProduct.forEach(el => {
            el.list.forEach(el2 => {
                logger.write(`${el2.filename};${el2.total};${el.product}\n`)
            })
        })
        await logger.end();
        console.log('done cetak summary')
    }
}


function remove_plus_minus(data){
    if (data.includes('-')){
        data = data.replace('-', ' CR')
    }else{
        data = data.replace('+', '')
    }
    // return data.replace('0 CR', '0')
    return data
}
function card_strip_format(card_no) {
    return card_no = `${card_no.substring(0, 4)}-${card_no.substring(4, 8)}-${card_no.substring(8, 12)}-${card_no.substring(12, 16)}`;
}

module.exports = {
    layout: new LAYOUT(),
    func: new FUNC(),
    init: new INIT(),
    excel: new EXCEL(),
    summary: new SUMMARY(),
}

