const express = require('express');
const router = express.Router();
// const checkAuthEmployee = require('../../middleware/check-auth-employees')
// const checkAuthAdmin = require('../../middleware/check-auth-admin')
const url1 = '/apps';
const url2 = '/cms';
const url3 = '/web';


// region CMS
// ------------------------------------------------ CMS -----
// PATH FOLDER
// DOCUMENT
const cms_document = require('./cms/document');
// AUTH
const cms_auth= require('./cms/auth');
// USERS
const cms_users = require('./cms/users');
// USERS ADMIN
const cms_users_admin = require('./cms/users/admin');


// PATH URL/API
// DOCUMENT
router.use(`${url2}/document`, cms_document);
// AUTH
router.use(`${url2}/auth`, cms_auth);
// USERS
router.use(`${url2}/users`, cms_users);
// USERS ADMIN
router.use(`${url2}/users/admin`, cms_users_admin);
//endregion


// region APPS
// --- -------------------------------------------- APPS -----------------------------------------------
// PATH FOLDER
// PAYROLL
const apps_payroll = require('./apps/payroll');
// AUTH
const apps_auth = require('./apps/auth');
const apps_auth_employees = require('./apps/auth/employees');
// EMPLOYEES
const apps_employees = require('./apps/employees');

// PATH URL / API
// PAYROLL
router.use(`${url1}/payroll`, apps_payroll);
//region AUTH
router.use(`${url1}/auth`, apps_auth);
router.use(`${url1}/auth/employees`, apps_auth_employees);
// EMPLOYEES
router.use(`${url1}/employees`, apps_employees);

// endregion

// region APPS
// --- -------------------------------------------- HOME -----------------------------------------------
// PATH FOLDER
// HOME
const web_home = require('./web/home');

// PATH URL / API
// HOME
router.use(`${url3}/home`, web_home);

// endregion

module.exports = router;