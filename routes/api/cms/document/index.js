const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const slugify = require('slugify')
const path = require('path')
const multer  = require('multer')
const randomstring = require("randomstring");
const storage = multer.diskStorage(
    {
        destination: '/mnt/cdn/tni-files/assets/files/document',
        filename: function (req, file, cb) {
            let files = ''
            if (req.body.files === undefined) {
                files = randomstring.generate({
                    length: 20,
                    // capitalization: 'uppercase',
                });
            } else {
                files = req.body.image_url.replace(/\.[^/.]+$/, "")
            }
            // console.log(file.mimetype.split('/')[1])
            // console.log(path.extname(file.originalname))
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            // cb( null, Date.now()+`-${symbol}${path.extname(file.originalname)}`);
            cb(null, files + `${path.extname(file.originalname)}`);
        }
    }
);
let upload = multer({ storage: storage })


router.get('/', (req, res) => {
    res.send('We Are In DOCUMENT Route')
})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    db.select(
        '*',
    )
        .from('tb_document')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Document still empty' : "Success get data document",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET DOCUMENT BY ID
router.get('/list/id=:id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .where({'id': req.params.id})
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Document still empty' : "Success get data document",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .whereRaw(`CONCAT_WS('', ref_no, regarding, letter_type, agenda_no, attachment, receive_from, hit_on_last_number, hit_on_the_next_number, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get document' : 'There are no document yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});

// ADD
router.post('/add', upload.single('files'), (req, res) => {
    req.body.slug = slugify(req.body.regarding.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    req.body.agenda_date ? null : req.body.agenda_date = moment().format('YYYY/MM/DD')
    req.body.letter_date ? null : req.body.letter_date = moment().format('YYYY/MM/DD')
    if (req.file) {
        // console.log('Uploading file... '+req.file.filename);
        // let filename = req.file.filename;
        // let uploadStatus = 'File Uploaded Successfully';
        req.body.files = `${req.file.filename}`
        // req.body.expired_at = moment().utc().add('1', 'year').format('YYYY-MM-DD HH:mm:ss')
        // console.log(moment().utc().add('1', 'year').format('YYYY-MM-DD HH:mm:ss'))
        db.select(
            'agenda_no',
        )
            .from('tb_document')
            .where({
                agenda_no: req.body.agenda_no,
            })
            .then((data) => {
                if (data.length === 0){
                    // console.log(detail)
                    db('tb_document')
                        .insert(req.body)
                        .then(data => {
                            res.json({
                                success: true,
                                message: 'Add document success',
                                count: data,
                                // data: result,
                            });
                        })
                    return true
                }else{
                    res.json({
                        success: false,
                        message: 'Document already exist',
                        // count: data,
                        // data: result,
                    });

                }
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Add document failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "upload image failed",
        });
    }
});

// UPDATE
router.post('/update', upload.single('updated_files'), (req, res) =>  {
    // console.log(req.file)
    req.body.slug = slugify(req.body.regarding.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    // req.body.images = req.body.image_url
    delete req.body.image_url
    db('tb_document')
        .where('id', req.body.id)
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update banners success.",
                // count: data.length,
                data: data,
            });
        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update banners failed.",
            // count: data.length,
            data: err,
        });
    });
});

// UPDATE IS PUBLISH
router.post('/update-is-publish', (req, res) =>  {
    // console.log(req.body)
    db('banners')
        .where('id', req.body.id)
        .update('is_publish', req.body.is_publish)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status succedd.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banners status failed",
                // count: data.length,
                data: err,
            });
        });
});

// UPDATE IS ACTIVE
router.post('/update-is-active', (req, res) =>  {
    // console.log(req.body)
    db('banners')
        .where('id', req.body.id)
        .update('is_deleted', req.body.is_deleted)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banner status failed",
                // count: data.length,
                data: err,
            });
        });

});

// DELETE
router.delete('/delete/:id', (req, res) => {

    db('tb_document').where({ id: req.params.id })
        .del()
        .then((data) => {
        res.json({
            success: true,
            message: "Delete document success",
            data: data,
        });
    });
});

module.exports = router;