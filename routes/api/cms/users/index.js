const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const checkAuth = require('../../../../middleware/check-auth')
const date = require('../../../../plugins/moment-date-format')
const slugify = require('slugify')
const CryptoJS = require("crypto-js");
const changeCase = require('change-case')
// let data = firedb.ref('progress-loading')
// data.on('value', function(snapshot) {
//     console.log(snapshot.val())
//     // self.chatMessag`es = snapshot.val();
// });
router.get('/', (req, res) => {
    res.send('We Are In USERS Route')
})

//region USERS
// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
        't2.name as organization_name',
    )
        .from('users as t1')
        .innerJoin('master_organization as t2', 't1.id_organization', 't2.id')
        // .where(listWhere)
        .orderBy('t1.'+req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator.data[1].birthdate)
            // const age = moment().diff(paginator.data[1].birthdate, 'years');
            // console.log(age)
            res.json({
                success: true,
                message: paginator.data.length === 0 ? 'Users masih kosong.' :"Success get data users",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch(err => {
            console.log(err)
            res.json({
                success: false,
                message: err,
            });
        })
});

// GET BY ID
router.get('/list/id=:id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    // console.log(req.params)
    // let user_id = req.userData.id
    let sort = req.params.sort
    let page = req.params.page
    let limit = req.params.limit
    // console.log(listWhere)
    // let remember_token = req.body.remember_token
    db.select(
        't1.*'
    )
        .from('users as t1')
        .where({'t1.id': req.params.id})
        .orderBy('t1.id', sort)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "User tidak ditemukan.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data users by id",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});

// GET LIST COLUMN FILTER
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort/column-filter=:column_filter', (req, res) => {
    // console.log(column_filter)
    let resultDec = {
        't1.is_deleted': '0',
        't1.id_organization': 5,
    }
    if (req.params.column_filter !== 'null'){
        // decrypt
        let reb64 = CryptoJS.enc.Hex.parse(req.params.column_filter);
        let bytes = reb64.toString(CryptoJS.enc.Base64);
        let decrypt = CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
        resultDec = JSON.parse(decrypt.toString(CryptoJS.enc.Utf8))
        // var plain = decrypt.toString(CryptoJS.enc.Utf8);
    }
    const entries = Object.entries(resultDec)
    const q = db.select(
        't1.*',
        't2.name as organization_name'
    )
        .from('users as t1')
        .innerJoin('master_organization as t2', 't1.id_organization', 't2.id');
    entries.forEach((element) => {
        // console.log(element[0], element[1])
        if (element[0] === 'email' || element[0] === 'name'){
            q.where('t1.'+element[0], 'like', `%${element[1]}%`)
        }else{
            if (element[0] === 'organization_name'){
                if (element[1] !== ''){
                    q.where('t2.name', element[1])
                }
            }else if (element[1] !== ''){
                q.where(element[0], element[1])
            }
        }
    })
    q.orderBy(`${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data users",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sort,
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
        't2.name as organization_name'
    )
        .from('users as t1')
        .innerJoin('master_organization as t2', 't1.id_organization', 't2.id')
        .whereRaw(`CONCAT_WS('', t2.name, t1.email, t1.name, t1.phone) LIKE ?`, [`%${req.params.value}%`])
        .andWhere({'t1.is_deleted': '0', 't2.is_deleted' : '0'})
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data users",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sort,
                data: paginator.data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get list organization failed.",
                data: err,
            });
            console.log('organization : '+err)
        });
});

// ADD
router.post('/add', (req, res) => {
    // console.log(req.body)
    // console.log(req.file)
    req.body.slug = slugify(req.body.name, {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    req.body.name = changeCase.titleCase(req.body.name)
    db.select(
        'name',
    )
        .from('master_account_type')
        .where({
            name: req.body.name,

        })
        .orWhere({name: req.body.name})
        .then((data) => {
            if (data.length === 0){
                // console.log(detail)
                db('master_account_type')
                    .insert(req.body)
                    .then(data => {
                        res.json({
                            success: true,
                            message: 'Add account type success',
                            count: data,
                            // data: result,
                        });
                    })

            }else{
                res.json({
                    success: false,
                    message: 'Account type name already exist ',
                    count: data,
                    // data: result,
                });
            }
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Add Account type failed.",
                // count: data.length,
                data: err,
            });
        });
});

// UPDATE IS ACTIVE
router.post('/update-is-verified-org', (req, res) =>  {
    db('users')
        .where('id', req.body.id)
        .update('is_verified_org', req.body.is_verified_org)
        .then(data => {
            res.json({
                success: true,
                message: "Update verified organization status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update organization status failed",
                // count: data.length,
                data: err,
            });
        });

});


//endregion

// GET
router.get('/list/sampel/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    // console.log(req.params)
    // let user_id = req.userData.id
    let sort = req.params.sort
    let page = req.params.page
    let limit = req.params.limit
    // console.log(listWhere)
    // let remember_token = req.body.remember_token
    db.select(
        '*'
    )
        .from('members')
        // .where(listWhere)
        .orderBy('id', sort)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Users masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data users",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});

module.exports = router;