
exports.up = function(knex, Promise) {
    return knex.schema.createTable('tb_document', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.integer('agenda_no')
            .unique()
            .comment('Nomor Agenda | format angka : 1 2 3 4')
        table.string('ref_no')
            .unique()
            .comment('Nomor Surat | format : B/123/IV/2021/Sintel')
        table.enum('letter_type', ['SURAT', 'NOTA DINAS', 'TR', 'ST', 'SPRIN', 'UNDANGAN'])
            .comment('Jenis Surat')
        table.dateTime('agenda_date')
            .comment('Tanggal Agenda')
        table.dateTime('letter_date')
            .comment('Tanggal Surat')
        table.string('attachment')
            .comment('Lampiran | format : 1 Lembar/Berkas')
        table.string('receive_from')
            .comment('Terima Dari')
        table.string('regarding')
            .comment('PERIHAL')
        table.string('hit_on_last_number')
            .comment('Petunjuk pada nomor yang lalu')
        table.string('hit_on_the_next_number')
            .comment('Petunjuk pada nomor yang berikutnya')
        table.string('description')
            .comment('Keterangan')
        table.string('slug')
            .comment('SLUG')
        table.string('files')
            .comment('File')
        table.specificType('is_deleted', 'tinyint(1)')
            .comment('Status Delete')
            .notNullable()
            .defaultTo(0);
        table.dateTime('created_at')
            .comment('Tanggal Di Buat')
            .notNullable()
            .defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at')
            .comment('Tanggal Update')
            .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('tb_document')
};
